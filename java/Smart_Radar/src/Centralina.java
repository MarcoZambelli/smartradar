import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

/**
 * Test Serial Monitor, based on: 
 * http://playground.arduino.cc/Interfacing/Java
 *
 */
public class Centralina extends BasicEventLoopController {
	
	enum State {IDLE, SCANNING, TRACKING};
	Light[] leds = new Led[3];
	Button[] buttons = new Button[2];
	ButtonReleased btnOn;
	ButtonPressed btnOff;
	SerialMonitor monitor;
	State s;
	final static double DIST_MIN = 0.2;
	final static double DIST = 1.0;
	int nObj = 0;
	
	public Centralina(SerialMonitor monitor){
		this.monitor = monitor;
		s = State.IDLE;
		leds[0] = new Led(0); 
		leds[1] = new Led(2);
		leds[2] = new Led(3);
		buttons[0] = new Button(4);
		buttons[1] = new Button(5);
		buttons[0].addObserver(this);
		buttons[1].addObserver(this);
		monitor.setCentralina(this);
		this.run();
	}
	
	@Override
	protected void processEvent(Event ev) {
		try (PrintWriter w = new PrintWriter(new FileWriter("log.txt",true))){
			switch(s){
				case IDLE:
					if(ev instanceof ButtonPressed){
						if(((ButtonPressed) ev).getSourceButton().equals(buttons[0])){
							leds[0].switchOn();
							s = State.SCANNING;
							monitor.sendMessage("ON");
						}			
					}
					break;
				case SCANNING:
					if(ev instanceof SerialEvent){
						String[] text = monitor.getText().split(" ");
						if(Integer.parseInt(text[1]) == 0){
							System.out.println("Scanning complete, object rilevated : " + nObj);
							w.append("Scanning complete, object rilevated : " + nObj);
							nObj = 0;
						}
						if(Double.parseDouble(text[0]) > DIST_MIN && Double.parseDouble(text[0]) < DIST){
							nObj ++;
							leds[1].switchOn();
							Thread.sleep(100);
							leds[1].switchOff();
							System.out.println("Time " + Calendar.getInstance().getTime() + " - Object detected at angle " + text[1]);	
							w.append("Time " + Calendar.getInstance().getTime() + " - Object detected at angle " + text[1]);
						}
						if(Double.parseDouble(text[0]) <= DIST_MIN){
							nObj ++;
							s = State.TRACKING;
							monitor.sendMessage("TRACKING");
							leds[2].switchOn();
							System.out.println("Time " + Calendar.getInstance().getTime() + " - Object tracked at angle " + text[1] + " distance " + text[0]);
							w.append("Time " + Calendar.getInstance().getTime() + " - Object tracked at angle " + text[1] + " distance " + text[0]);
						}
					}
					if(ev instanceof ButtonPressed){
						if(((ButtonPressed) ev).getSourceButton().equals(buttons[1])){
							leds[0].switchOff();
							s = State.IDLE;
							monitor.sendMessage("OFF");
						}
					}
					break;
				case TRACKING:
					if(ev instanceof SerialEvent){
						String[] text = monitor.getText().split(" ");
						if(Double.parseDouble(text[0]) > DIST_MIN){
							leds[2].switchOff();
							monitor.sendMessage("SCANNING");
							s = State.SCANNING;
						}
					}
					if(ev instanceof ButtonPressed){
						if(((ButtonPressed) ev).getSourceButton().equals(buttons[1])){
							leds[0].switchOff();
							leds[2].switchOff();
							s = State.IDLE;
							monitor.sendMessage("OFF");
						}
					}
					break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}		
}