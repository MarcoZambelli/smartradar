
public class Launcher {
	public static void main(String[] args) throws Exception {
		String comPortName = "/dev/ttyS99";
		int dataRate = 9600;
		System.out.println("Start monitoring serial port "+comPortName+" at boud rate: "+dataRate);
		try {
			SerialMonitor monitor = new SerialMonitor();
			monitor.start(comPortName,dataRate);
			new Centralina(monitor);
				
			Thread.sleep(1000000);
			
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}
}
