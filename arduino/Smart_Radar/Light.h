#ifndef __LIGHT__
#define __LIGHT__

class Light {
public:
  virtual void switchOn() = 0;
  virtual void switchOff() = 0;  
  virtual void setIntensity(int) = 0;
  virtual int getIntensity() = 0;
};

#endif

