#ifndef __RADARTASK__
#define __RADARTASK__

#include "Task.h"
#include "Led.h"
#include "Sonar.h"
#include "Timer.h"
#include "Servo.h"

class RadarTask: public Task {
  int pin[4];
  Light* led;
  ProximitySensor* prox;
  Servo servo;
  enum State { WAITING, SCANNING, TRACKING } state;

public:

  RadarTask(int pinLed, int pinProxEcho, int pinProxTrig, int pinServo);
  void init(int period);
  void tick();
};

#endif
