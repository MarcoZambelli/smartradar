#include "RadarTask.h"
#include "Scheduler.h"
#define PARKING_TASK_PERIOD 100
#define SCHEDULER_PERIOD 100
#define PROX_ECHO_PIN 13
#define PROX_TRIG_PIN 12
#define LED_PIN 9
#define SERVO_PIN 5

Scheduler sched;

void setup() {
  Serial.begin(9600);
  
  sched.init(SCHEDULER_PERIOD);

  Task* t0 = new RadarTask(LED_PIN, PROX_ECHO_PIN, PROX_TRIG_PIN, SERVO_PIN);
  t0->init(PARKING_TASK_PERIOD);
  sched.addTask(t0);
}

void loop() {
  sched.run();
}
