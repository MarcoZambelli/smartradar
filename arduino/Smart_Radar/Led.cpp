#include "Led.h"
#include "Arduino.h"

Led::Led(int pin){
  this->pin = pin;
  pinMode(pin,OUTPUT);
  currentIntensity = 1;
}

Led::Led(int pin, int intensity){
  this->pin = pin;
  pinMode(pin,OUTPUT);
  currentIntensity = intensity;
}

void Led::switchOn(){
  analogWrite(pin,HIGH);
}

void Led::switchOff(){
  analogWrite(pin,LOW);
}

void Led::setIntensity(int intensity){
  currentIntensity = intensity;
  analogWrite(pin,currentIntensity);
}

int Led::getIntensity(){
  return this->currentIntensity;
}



