#ifndef __LED__
#define __LED__

#include "Light.h"

class Led: public Light { 
public:
  Led(int pin);
  Led(int pin, int intensity);
  void switchOn();
  void switchOff();
  void setIntensity(int i);
  int getIntensity();
private:
  int pin;
  int currentIntensity; 
};

#endif
