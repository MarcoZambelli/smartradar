#include "RadarTask.h"
#include "Arduino.h"
#define OMEGA 10
#define DIST_RADAR 1.0

int servoAngle = 90;
bool servoRotate = true;
String command;
float distance;

RadarTask::RadarTask(int ledPin, int proxEchoPin, int proxTrigPin, int servoPin){
  this->pin[0] = ledPin;
  this->pin[1] = proxEchoPin;
  this->pin[2] = proxTrigPin;
  this->pin[3] = servoPin;
}

void RadarTask::init(int period){
  Task::init(period);
  led = new Led(pin[0]);
  prox = new Sonar(pin[1], pin[2]);
  servo.attach(pin[3]);
  state = WAITING;
}

void RadarTask::tick(){
  if(Serial.available() > 0){
    command = Serial.readString();
  }
  distance = prox->getDistance();
  switch(state){
    case WAITING:
      servo.write(servoAngle);
      if(command == "ON"){
        state = SCANNING;
        led->setIntensity(255);
        servoAngle = 90;
        servoRotate = true;
        command = "";
      }
      break;
    case SCANNING:
      if(command == "OFF"){
        led->setIntensity(0);
        servoAngle = 90;
        state = WAITING;
        command = "";
      }
      if(distance < DIST_RADAR){   
        String s = "";   
        s += distance;
        s += " ";
        s += servoAngle;
        Serial.println(s);
        if(command == "TRACKING"){
          state = TRACKING;
          command = "";
        }
      }
      else{
        String s = "";
        s += DIST_RADAR;
        s += " ";
        s += servoAngle;
        Serial.println(s);
      } 
      if(state == SCANNING){ 
        if(servoRotate){
          servoAngle += OMEGA;
          if(servoAngle == 180){
            servoRotate = false;
          }
        }
        else{
          servoAngle -= OMEGA;
          if(servoAngle == 0){
            servoRotate = true;
          }
        }
        servo.write(servoAngle);
      }
      break;
    case TRACKING:
      if(command == "OFF"){
        led->setIntensity(0);
        servoAngle = 90;
        state = WAITING;
        command = "";
      }
      if(command == "SCANNING"){
        state = SCANNING;
        command = "";
      }
      String s = "";   
      s += distance;
      s += " ";
      s += servoAngle;
      Serial.println(s);
      break;
  }
}

